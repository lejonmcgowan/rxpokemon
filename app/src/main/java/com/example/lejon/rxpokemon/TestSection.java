package com.example.lejon.rxpokemon;

import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.flexibleadapter.items.AbstractSectionableItem;
import eu.davidea.flexibleadapter.items.ISectionable;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by lejon on 7/5/2017.
 */

class TestSection extends AbstractFlexibleItem<TestSection.SectionHolder> implements ISectionable<TestSection.SectionHolder,TestExpandable>
{
    private final String id;
    private TestExpandable header;

    public TestSection(TestExpandable header, String id)
    {
       super();
        this.header = header;
        this.id = id;
    }


    @Override
    public boolean equals(Object o)
    {
        return o instanceof TestSection && id.equals(((TestSection) o).id);
    }

    @Override
    public int getLayoutRes()
    {
        return R.layout.layout_simple_item;
    }

    @Override
    public SectionHolder createViewHolder(View view, FlexibleAdapter adapter)
    {
        return new SectionHolder(view,adapter);
    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, SectionHolder holder, int position, List payloads)
    {
        holder.name.setText("SECTION " + position);
    }

    @Override
    public TestExpandable getHeader()
    {
        return header;
    }

    @Override
    public void setHeader(TestExpandable header)
    {

    }

    public class SectionHolder extends FlexibleViewHolder
    {
        @BindView(R.id.name) TextView name;
        public SectionHolder(View view, FlexibleAdapter adapter)
        {
            super(view, adapter);
            ButterKnife.bind(this,view);
        }
    }
}
