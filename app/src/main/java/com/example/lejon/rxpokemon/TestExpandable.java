package com.example.lejon.rxpokemon;

import android.graphics.Color;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractExpandableItem;
import eu.davidea.flexibleadapter.items.IHeader;
import eu.davidea.viewholders.ExpandableViewHolder;

/**
 * Created by lejon on 7/5/2017.
 */

public class TestExpandable extends AbstractExpandableItem<TestExpandable.ExpandableHolder,TestSection>
        implements IHeader<TestExpandable.ExpandableHolder>
{

    private String id;
    public TestExpandable(String id)
    {
        this.id = id;
        setEnabled(true);
    }

    public class ExpandableHolder extends ExpandableViewHolder
    {
        @BindView(R.id.root) View root;
        @BindView(R.id.name) TextView nameField;


        public ExpandableHolder(View view, FlexibleAdapter adapter)
        {
            super(view, adapter);
            ButterKnife.bind(this,view);
        }
    }


    @Override
    public boolean equals(Object o)
    {
        return o instanceof TestExpandable && id.equals(((TestExpandable)o).id);
    }

    @Override
    public int hashCode()
    {
        return id.hashCode();
    }

    @Override
    public int getLayoutRes()
    {
        return R.layout.layout_simple_item_2;
    }

    @Override
    public ExpandableHolder createViewHolder(View view, FlexibleAdapter adapter)
    {
        return new ExpandableHolder(view,adapter);
    }

    @Override
    public void unbindViewHolder(FlexibleAdapter adapter, ExpandableHolder holder, int position)
    {

    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ExpandableHolder holder, int position, List payloads)
    {
        holder.nameField.setBackgroundColor(Color.parseColor("#555500"));
        holder.nameField.setText("IM A HEADER");
    }


}
