package com.example.lejon.rxpokemon;

import android.content.Context;
import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lejon on 7/6/2017.
 */

class MyVolleyBlockingTask extends AsyncTask<String,String, Integer>
{
    private Context ctx;
    public int result;
    public MyVolleyBlockingTask(Context context)
    {
        ctx = context;
    }

    @Override
    protected Integer doInBackground(String... params) {

        // Method runs on a separate thread, make all the network calls you need
        TestVolley tester = new TestVolley();

        JSONObject jsonObject = tester.fetchModules(ctx);
        try
        {
            return jsonObject.getInt("count");
        }
        catch (JSONException e)
        {
            e.printStackTrace();
            return -1;
        }
    }


    @Override
    protected void onPostExecute(Integer result)
    {
        // runs on the UI thread
        // do something with the result
        this.result = result;
    }

    public Status check()
    {
        return getStatus();
    }
}
