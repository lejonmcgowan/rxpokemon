package com.example.lejon.rxpokemon;

import android.os.AsyncTask;
import android.support.v4.util.Pair;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.pacoworks.rxpaper2.RxPaperBook;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity
{

    @BindView(R.id.pokemon_list)
    RecyclerView pokemonList;
    private FlexibleAdapter<AbstractFlexibleItem> adapter;
    private final int[] numPokemon = new int[1];

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        adapter = new FlexibleAdapter<>(new ArrayList<AbstractFlexibleItem>());
        pokemonList
                .setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        pokemonList.setAdapter(adapter);


        RxPaperBook.init(this);
        RxPaperBook book = RxPaperBook.with();

        //attempt to find number in RXPaper dataabse
        //more info on how to use Paper here:
        //https://github.com/pakoito/RxPaper2
        Single<Integer> countRead = book.read("count");

        countRead.subscribe(new SingleObserver<Integer>()
        {
            @Override
            public void onSubscribe(@NonNull Disposable d)
            {

            }

            @Override
            public void onSuccess(@NonNull Integer integer)
            {
                numPokemon[0] = integer;
                Log.i("DATABASE FETCH", "number received from database");
                fetchPokemon();
            }

            @Override
            public void onError(@NonNull Throwable e)
            {
                //default to network method
                /**
                 * Task 1: Store this number received from the network in this method into RxPaper, and check to make sure OnSuccess is called in subsequent calls
                 * the network log in the mathod should (normally) not appear in subsequent runs of the app
                 */
                retrievePokemonNumberFromNetwork();
            }
        });

    }

    private void fetchPokemon()
    {
        //now, setup for the netowrk request of the pokemon data we need

        // Instantiate the cache
        Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024); // 1MB cap
        // Set up the network to use HttpURLConnection as the HTTP client.
        Network network = new BasicNetwork(new HurlStack());

        // Instantiate the RequestQueue with the cache and network.
        final RequestQueue pokemonQueue = new RequestQueue(cache, network);

        //make the request URLS
        ArrayList<Pair<String, Integer>> requestList = new ArrayList<>();
        for (int i = 1; i <= numPokemon[0]; i++)
        {
            requestList.add(new Pair<>("http://pokeapi.co/api/v2/pokemon/" + i + "/", i));
        }


        /**
         * Task 2:
         * setup Rx here with 3 differnt subscriptions
         *
         * 1. a simple list of all the pokemon, along with their types. Ordered by their ID number
         * 2. A list of all fairy type pokemon only. ALso ordered by ID
         * 3. an 'empty' stream. It doesn't care about items, but you want this to log a completion message.
         *
         * this will help you understand how to filter items in subscriptions. see here for some of the operations and it's descriptions
         * https://github.com/ReactiveX/RxJava/wiki/Filtering-Observables
         *
         * to save time, you may also want to look into using subjects to stream in and out data using Subjects:
         *
         * https://github.com/Froussios/Intro-To-RxJava/blob/master/Part%201%20-%20Getting%20Started/2.%20Key%20types.md#subject
         *
         * (keep in mind that this is in RX1, so you may need tro translate some things (e.g. Action1 -> Single)
         */


        //when Rx is ready, start the nework queue
        pokemonQueue.start();
        for (final Pair<String, Integer> reqURL : requestList)
        {
            StringRequest request = new StringRequest(Request.Method.GET, reqURL.first, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    /**
                     * task 3: Parse the respone (inb JSON) to retrieve the pokemon, it's number, and it's
                     * type(s), then push the response to your stream
                     */
                    Log.i("Pokemon Request", "request " + reqURL.second);

                }
            }, new Response.ErrorListener()
            {
                @Override
                public void onErrorResponse(VolleyError error)
                {
                    Log.e("Pokemon Request fail", error.toString());
                }
            });

            request.setRetryPolicy(new DefaultRetryPolicy(2000,10,DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            pokemonQueue.add(request);
        }

        /**
         * Task 4: want to add another subscription now. This one filters based on Gen1 and Gen2 pokemon (id <= 251)
         *
         * you may have to change your structure above so that it caches the results the stream emmited. e.g.
         *
         * http://www.vogella.com/tutorials/RxJava/article.html#caching-values-of-completed-observables
         */

    }

    private void retrievePokemonNumberFromNetwork()
    {
        final int[] num = new int[1];
        VolleyLog.DEBUG = true;
        RequestQueue queue = Volley.newRequestQueue(this);
        JsonObjectRequest jsonResult = new JsonObjectRequest(Request.Method.GET, "http://pokeapi.co/api/v2/pokemon/", new JSONObject(), new Response.Listener<JSONObject>()
        {
            @Override
            public void onResponse(JSONObject response)
            {
                try
                {
                    numPokemon[0] = response.getInt("count");
                    Log.i("NETWWORK FETCH SUCCESS", "fetch number: " + numPokemon[0]);
                    fetchPokemon();
                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error)
            {
                Log.e("QUERY FAILED", "Still hating life: " + error.getMessage());
            }
        });
        jsonResult.setRetryPolicy(new DefaultRetryPolicy());
        queue.add(jsonResult);
    }
}
