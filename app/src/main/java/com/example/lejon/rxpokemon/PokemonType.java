package com.example.lejon.rxpokemon;

import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;

/**
 * Created by lejon on 7/3/2017.
 */

public enum PokemonType
{
    WATER("water"),
    FIRE("fire"),
    GRASS("grass"),
    GROUND("ground"),
    ROCK("rock"),
    STEEL("steel"),
    ICE("ice"),
    ELECTRIC("electric"),
    DRAGON("dragon"),
    GHOST("ghost"),
    PSYCHIC("psychic"),
    NORMAL("normal"),
    FIGHTING("fighting"),
    POISON("poison"),
    BUG("bug"),
    FLYING("flying"),
    DARK("dark"),
    FAIRY("fairy"),
    NONE("none");

    private String type;


    PokemonType(String type)
    {
        this.type = type.toUpperCase();
    }

    public @DrawableRes int getIcon()
    {
        switch(toString().toUpperCase())
        {
            case "WATER": return R.drawable.water_icon;
            case "FIRE": return R.drawable.fire_icon;
            case "GRASS": return R.drawable.grass_icon;
            case "GROUND": return R.drawable.ground_icon;
            case "ROCK": return R.drawable.ground_icon;
            case "STEEL": return R.drawable.steel_icon;
            case "ICE": return R.drawable.ice_icon;
            case "ELECTRIC": return R.drawable.electric_icon;
            case "DRAGON": return R.drawable.dragon_icon;
            case "GHOST": return R.drawable.ghost_icon;
            case "PSYCHIC": return R.drawable.psychic_icon;
            case "NORMAL": return R.drawable.normal_icon;
            case "FIGHTING": return R.drawable.fighting_icon;
            case "POISOIN": return  R.drawable.poison_icon;
            case "BUG": return R.drawable.bug_icon;
            case "FLYING": return R.drawable.flying_icon;
            case "DARK": return R.drawable.dark_icon;
            case "FAIRY": return R.drawable.fairy_icon;
            default: return 0;
        }
    }

    @Override
    public String toString()
    {
        return type;
    }
}
