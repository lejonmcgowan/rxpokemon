package com.example.lejon.rxpokemon;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import eu.davidea.flexibleadapter.FlexibleAdapter;
import eu.davidea.flexibleadapter.items.AbstractFlexibleItem;
import eu.davidea.viewholders.FlexibleViewHolder;

/**
 * Created by lejon on 7/3/2017.
 */

public class PokemonItem extends AbstractFlexibleItem<PokemonItem.ViewHolder>
{
    public PokemonItem(String name, int pokeID, PokemonType type1, PokemonType type2)
    {
        this.name = name;
        this.type1 = type1;
        this.type2 = type2;
        this.pokeID = pokeID;
    }

    String name;
    PokemonType type1, type2;
    int pokeID;

    public class ViewHolder extends FlexibleViewHolder
    {
        @BindView(R.id.name) TextView nameField;
        @BindView(R.id.type_1) ImageView type1Field;
        @BindView(R.id.type_2) ImageView type2Field;

        public ViewHolder(View view, FlexibleAdapter adapter)
        {
            super(view, adapter);
        }
    }

    @Override
    public boolean equals(Object o)
    {
        return o instanceof PokemonItem && name.equals(((PokemonItem) o).name);
    }

    @Override
    public int getLayoutRes()
    {
        return R.layout.layout_pokemon_item;
    }

    @Override
    public ViewHolder createViewHolder(View view, FlexibleAdapter adapter)
    {
        return new ViewHolder(view,adapter);
    }

    @Override
    public void bindViewHolder(FlexibleAdapter adapter, ViewHolder holder, int position, List payloads)
    {
        holder.nameField.setText(name);
        holder.type1Field.setImageResource(type1.getIcon());
        holder.type2Field.setImageResource(type2.getIcon());
    }
}
